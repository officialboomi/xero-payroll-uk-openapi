// Copyright (c) 2022 Boomi, Inc.

package com.boomi.connector.xero_payroll_uk;

import com.boomi.connector.api.OperationContext;
import com.boomi.connector.openapi.OpenAPIOperationConnection;

public class CustomOperationConnection extends OpenAPIOperationConnection {

    public CustomOperationConnection(OperationContext context) {
        super(context);
    }
}
